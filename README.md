# bfc


## Description

This is an example of a grpc service that brutes the password for a zip file in parallel

## Installation

```bash
pip install -r requirements.txt
```

## Usage

server
```bash
python server.py
```

client
```bash
python bruteforce.py -p passwords.txt test.zip   
```
