from __future__ import print_function

import argparse
import logging
from Queue import PriorityQueue
from threading import Thread

import grpc
import checker_pb2
import checker_pb2_grpc

SERVER_HOST = 'localhost:7070'
task_queue = PriorityQueue()


class Task(object):
    def __init__(self, password_filename, zip_filename, priority=1):
        self.priority = priority
        self.password_filename = password_filename
        self.zip_filename = zip_filename
        self.result = None

    def __cmp__(self, other):
        return cmp(self.priority, other.priority)


class BruteForceClient(object):

    @staticmethod
    def check_passwords(password_filename, zip_filename):
        with grpc.insecure_channel(target=SERVER_HOST) as channel:
            stub = checker_pb2_grpc.CheckerStub(channel)
            response = stub.check_passwords(checker_pb2.Pair(passwords=password_filename, zip=zip_filename))
            return response.result


def bruteforce_worker(queue):
    while True:
        task = queue.get()
        task.result = BruteForceClient.check_passwords(task.password_filename, task.zip_filename)
        queue.task_done()


def is_file_exist(filepath):
    try:
        with open(filepath):
            pass
    except IOError:
        return False
    return True


def validate_files(password_filename, zip_filename):
    if not is_file_exist(password_filename):
        print("{} not found".format(password_filename))
        return False

    if not is_file_exist(zip_filename):
        print("{} not found".format(zip_filename))
        return False

    return True


def run(password_filename, zip_filename):
    if not validate_files(password_file, zip_file):
        return

    task = Task(password_filename, zip_filename)
    task_queue.put(task)

    worker_thread = Thread(target=bruteforce_worker, args=(task_queue,))
    worker_thread.daemon = True
    worker_thread.start()

    task_queue.join()
    print("Password: " + task.result)


if __name__ == '__main__':
    logging.basicConfig()

    parser = argparse.ArgumentParser()

    parser.add_argument("-p", dest="passwords")
    parser.add_argument("zip")

    args = parser.parse_args()

    password_file = args.passwords
    zip_file = args.zip

    run(password_file, zip_file)
