from __future__ import print_function

import logging
import multiprocessing as mp
import os
import signal
import subprocess
import sys
import time

import grpc
from concurrent import futures

import checker_pb2
import checker_pb2_grpc

log = logging.getLogger(__name__)

PROCESS_COUNT = mp.cpu_count()
MAX_WORKERS = 2
CHUNK_SIZE = 16
LISTEN_PORT = 7070
ONE_HOUR = 60 * 60

pool = None


class Checker(checker_pb2_grpc.CheckerServicer):

    def check_passwords(self, request, context):
        log.debug('Determining pass of %s %s', request.passwords, request.zip)

        passwords_filename = request.passwords
        zip_filename = request.zip

        tasks = create_tasks(zip_filename, passwords_filename)
        result = process_tasks_results(tasks)

        log.debug(result)

        return checker_pb2.PasswordResult(result=str(result) or 'password not found')


def process_tasks_results(tasks):
    """Get finish result from pools tasks"""
    result = ''
    for task in tasks:
        chunks_results = task.get()
        result_pair = next((x for x in chunks_results if x[0]), None)
        if result_pair:
            _, _, result = result_pair
            break
    return result


def create_tasks(zip_filename, passwords_filename):
    """Create and pass tasks for pools workers"""
    tasks = []
    for start_pos, count_bytes in chunks_file(passwords_filename, CHUNK_SIZE):
        tasks.append(
            pool.apply_async(check_passwords_by_chunk, (zip_filename, passwords_filename, start_pos, count_bytes,)))
    return tasks


def check_passwords_by_chunk(zip_filename, passwords_filename, start_pos, count_bytes):
    """Try passwords from chunk"""
    results = []
    with open(passwords_filename, 'r') as f:
        f.seek(start_pos, 0)
        lines = f.read(count_bytes).splitlines()
        for line in lines:
            results.append(check_password_for_zip(zip_filename, line))
    return results


def check_password_for_zip(zip_filename, password):
    """Check password for zip via 7z and subprocess"""
    try:
        subprocess.check_output(["7z", "t", "-p{}".format(password), zip_filename], stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError:
        return False, zip_filename, password
    return True, zip_filename, password


def chunks_file(filename, chunk_size):
    """Generator - partition file data by chunk_size"""
    file_end = os.path.getsize(filename)
    with open(filename, 'r') as f:
        end_chunk = f.tell()
        while True:
            start_chunk = end_chunk
            f.seek(chunk_size, 1)
            f.readline()
            end_chunk = f.tell()
            count_bytes = end_chunk - start_chunk
            yield start_chunk, count_bytes
            if end_chunk > file_end:
                break


def _wait_forever(server):
    try:
        while True:
            time.sleep(ONE_HOUR)
    except KeyboardInterrupt:
        server.stop(None)


def init_worker():
    """Init pool process worker - ignore signal SIGINT"""
    signal.signal(signal.SIGINT, signal.SIG_IGN)


def run():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=MAX_WORKERS))
    checker_pb2_grpc.add_CheckerServicer_to_server(Checker(), server)
    server.add_insecure_port('[::]:{}'.format(LISTEN_PORT))

    global pool
    pool = mp.Pool(PROCESS_COUNT, init_worker)

    server.start()
    print('Listening on ::{}...'.format(LISTEN_PORT))

    _wait_forever(server)

    pool.close()
    pool.join()


if __name__ == '__main__':
    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('[PID %(process)d] %(message)s')
    handler.setFormatter(formatter)
    log.addHandler(handler)
    log.setLevel(logging.INFO)

    run()
